var platos = [
    {
        id: 1,
        nombre: "Papas a la Huancaina",
        precio: 15,
        imagen: "img/papas-huancaina.jpg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 2,
        nombre: "Pesque de Quinua",
        precio: 10,
        imagen: "img/pesque.jpg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 3,
        nombre: "Silpancho Cochabambino",
        precio: 10,
        imagen: "img/silpancho.jpg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 4,
        nombre: "Fricase",
        precio: 14,
        imagen: "img/fricase.jpg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 5,
        nombre: "Piqe Macho",
        precio: 17,
        imagen: "img/piqe-macho.jpg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 6,
        nombre: "Sopa de Fideo",
        precio: 10,
        imagen: "img/sopa-fideo.jpeg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 7,
        nombre: "Plato Paceño",
        precio: 15,
        imagen: "img/plato-paceño.jpeg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 8,
        nombre: "Majadito",
        precio: 16,
        imagen: "img/majadito.jpeg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 9,
        nombre: "Chorizo Chuquisaqueño",
        precio: 14,
        imagen: "img/chorizo.jpeg",
        pedidos: 0,
        precioTotal: 0,
    },
    {
        id: 10,
        nombre: "Charquekan",
        precio: 10,
        imagen: "img/charquekan.jpg",
        pedidos: 0,
        precioTotal: 0,
    }
];
var Indice = 0;
var plato = {};
var totalPagar = 0;

function siguiente() {
    Indice++;
    if (Indice > 9) Indice = 0;
    plato = platos[Indice];
    document.getElementById("cant-platos").value = plato.pedidos;
    document.getElementById("titulo").innerText = plato.nombre;
    document.getElementById("precio").innerText = plato.precio + "Bs";
    document.getElementById("imagen").src = plato.imagen;
}

function anterior() {
    Indice--;
    if (Indice < 0) Indice = 9;
    plato = platos[Indice];
    document.getElementById("cant-platos").value = plato.pedidos;
    document.getElementById("titulo").innerText = plato.nombre;
    document.getElementById("precio").innerText = plato.precio + "Bs";
    document.getElementById("imagen").src = plato.imagen;
}

function vender(indice) {
    Indice = indice;
    plato = platos[Indice];
    document.getElementById("cant-platos").value = plato.pedidos;
    document.getElementById("titulo").innerText = plato.nombre;
    document.getElementById("precio").innerText = plato.precio + "Bs";
    document.getElementById("imagen").src = plato.imagen;
}

function comprar() {
    if (Object.entries(plato).length === 0) plato = platos[Indice]; // si es la primera iteracion
    plato.pedidos++;
    plato.precioTotal = plato.pedidos * plato.precio;
    totalPagar += plato.precio;
    document.getElementById("cant-platos").value = plato.pedidos;
    document.getElementById("total-pagar").value = totalPagar;

    var pedidos = platos.filter(i => i.pedidos > 0);     // llenamos un array con solo los objetos que tengan pedidos>0
    var idPedidos = pedidos.map((pedido) => pedido.id);    // llenamos un arrary con solo 
    var cantPedidos = pedidos.map((pedido) => pedido.pedidos);

    document.getElementById("idPedidos").value = idPedidos;
    document.getElementById("cantPedidos").value = cantPedidos;
}

function devolver() {
    if (Object.entries(plato).length === 0) plato = platos[Indice];
    if (plato.pedidos === 0) {
        alert("No puedes hacer eso :(");
    } else {
        plato.pedidos--;
        plato.precioTotal = plato.pedidos * plato.precio;
        totalPagar -= plato.precio;
        document.getElementById("cant-platos").value = plato.pedidos;
        document.getElementById("total-pagar").value = totalPagar;

        var pedidos = platos.filter(i => i.pedidos > 0);     // llenamos un array con solo los objetos que tengan pedidos>0
        var idPedidos = pedidos.map((pedido) => pedido.id);    // llenamos un arrary con solo 
        var cantPedidos = pedidos.map((pedido) => pedido.pedidos);

        document.getElementById("idPedidos").value = idPedidos;
        document.getElementById("cantPedidos").value = cantPedidos;
    }
}

window.addEventListener('load', function () {
    this.document.getElementById("cant-platos").value = 0;
    this.document.getElementById("total-pagar").value = 0;
});