<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tarea 01</title>
    <link href="vendor/bootstrap-5.3.1-dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/logo.png">
</head>

<body>

    <?php
    class Plato
    {
        public $id;
        public $nombre;
        public $precio;
        public $pedidos;
        public function __construct($id, $nombre, $precio, $pedidos = 0)
        {
            $this->id = $id;
            $this->nombre = $nombre;
            $this->precio = $precio;
            $this->pedidos = $pedidos;
        }
    }

    $platos = [
        new Plato(1, "Papas a la Huancaina", 15),
        new Plato(2, "Pesque de Quinua", 10),
        new Plato(3, "Silpancho Cochabambino", 10),
        new Plato(4, "Fricase", 14),
        new Plato(5, "Piqe Macho", 17),
        new Plato(6, "Sopa de Fideo", 10),
        new Plato(7, "Plato Paceño", 15),
        new Plato(8, "Majadito", 17),
        new Plato(9, "Chorizo Chuquisaqueño", 14),
        new Plato(10, "Charquekan", 10),
    ];


    $idPedidos = $_POST['idPedidos'];
    $idPedidos = explode(",", $idPedidos[0]);
    $cantPedidos = $_POST['cantPedidos'];
    $cantPedidos = explode(",", $cantPedidos[0]);

    // var_dump($idPedidos);
    //var_dump($cantPedidos);
    ?>

    <div class="container mt-5">
        <div class="card">
            <div class="card-body">
                <table class="table">
                    <thead>

                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Plato</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Total</th>
                        </tr>

                    </thead>
                    <tbody>
                        <?php
                        $precioTotal = 0;
                        $count = 1;
                        $totalPagar = 0;

                        foreach ($platos as $key => $plato) {
                            if (in_array($plato->id, $idPedidos)) {  // verificamos si el palato tiene pedido
                                $i = 0;
                                foreach ($idPedidos as $key => $id) {
                                    $i = $plato->id == $id ? $key : 0;   // buscamos el :key (indice) dentro del array de pedidos
                                }
                                $totalPagar = $totalPagar + $plato->precio * $cantPedidos[$i];
                        ?>
                                <tr>
                                    <td><?= $count++ ?></td>
                                    <td><?= $plato->nombre ?></td>
                                    <td><?= $plato->precio ?></td>
                                    <td><?= $cantPedidos[$i] ?></td>
                                    <td><?= $plato->precio * $cantPedidos[$i] ?></td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                        <tr>
                            <td colspan="4" class="table-active fw-bold">TOTAL A PAGAR</td>
                            <td class="table-active fw-bold"> <?= $totalPagar ?> Bs</td>
                        </tr>
                    </tbody>
                </table>

                <button type="button" class="btn btn-dark" onclick="window.location.href='index.html'">Regresar al menu</button>
            </div>


        </div>
    </div>


    <script src="vendor/bootstrap-5.3.1-dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <script src="vendor/poppers/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="vendor/bootstrap-5.3.1-dist/js/bootstrap.min.js" integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa" crossorigin="anonymous"></script>
</body>

</html>