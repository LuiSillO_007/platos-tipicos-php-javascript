# venta de platos tipicos con Javascript y php
Este proyecto es una practica de carrito de ventas.

## Tecnologías Utilizadas
- HTML
- javascript
- [Bootstrap.js](https://getbootstrap.com/): version 5.3
- php

## Instalación

Sigue estos pasos para instalar y configurar el proyecto en tu entorno local:

1. Clona el repositorio a tu máquina local:

    ```bash
    git clone [url del repositorio]
    ```

2. Ingresa al directorio del proyecto:

    ```bash
    cd platos-tipicos-php-javascript
    ```

3. Instala las dependencias utilizando npm:

    ```bash
    npm install
    ```

## Capturas de pantalla:
### 1.- Ventana principal
![index](public/img/capturaIndex.png)

### 2.- Ventana de venta
![index](public/img/capturaCarrito.png)
